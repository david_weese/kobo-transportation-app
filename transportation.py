import urllib2
import pygame, os, time

import config

from pygame.locals import *
from xml.dom.minidom import parseString
from datetime import date, datetime, timedelta
from subprocess import call

os.environ['SDL_NOMOUSE'] = '1'
print("Kobo Wifi weather forecast started.")

en2de_dict = { "Monday":"Montag", "Tuesday":"Dienstag", "Wednesday":"Mittwoch", "Thursday":"Donnerstag", 
               "Friday":"Freitag", "Saturday":"Samstag", "Sunday":"Sonntag", 
               "Today":"heute", "Tomorrow":"morgen", "Current":"jetzt",
               "Last updated at":"Letzte Aktualisierung:" }

months = [ "Januar", "Februar", u"M\xe4rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" ]

def trans (en):
    if lang == "de":
        return en2de_dict.get(en, en)
    else:
        return en

def index_error(error):
    print(error)
    print("Failed to fetch weather data.")
    print("Double check your location settings by running:")
    print(" cat /mnt/onboard/.apps/koboWeather/location")
    print("If the information is incorrect, re-set your location with:")
    print(" /mnt/onboard/.apps/koboWeather/set_location.sh")
    
def convert_to_raw(surface):
    print("Converting image . . .")
    from cStringIO import StringIO
    file_str = StringIO()
    for col in range(surface.get_width()-1,-1,-1):
        for row in range(surface.get_height()):
            x = surface.get_at((col, row))
            file_str.write(chr(x[1])+chr(x[0]))
    f = open("/tmp/img.raw", "wb")
    f.write(file_str.getvalue())
    f.close()
    print("Image converted.")
    
class TextRectException:
    def __init__(self, message = None):
        self.message = message
    def __str__(self):
        return self.message

def render_textrect(string, font, rect, text_color, background_color, justification = 0, line_spacing = 0):
    """Returns a surface containing the passed text string, reformatted
    to fit within the given rect, word-wrapping as necessary. The text
    will be anti-aliased.

    Takes the following arguments:

    string - the text you wish to render. \n begins a new line.
    font - a Font object
    rect - a rectstyle giving the size of the surface requested.
    text_color - a three-byte tuple of the rgb value of the
                 text color. ex (0, 0, 0) = BLACK
    background_color - a three-byte tuple of the rgb value of the surface.
    justification - 0 (default) left-justified
                    1 horizontally centered
                    2 right-justified

    Returns the following values:

    Success - a surface object with the text rendered onto it.
    Failure - raises a TextRectException if the text won't fit onto the surface.
    """

    import pygame
    
    final_lines = []

    requested_lines = string.splitlines()

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    raise TextRectException, "The word " + word + " is too long to fit in the rect passed."
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    # Let's try to write the text out on the surface.

    surface = pygame.Surface(rect.size) 
    surface.fill(background_color) 

    accumulated_height = 0
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            break
        if line != "":
            tempsurface = font.render(line, 1, text_color)
            if justification == 0:
                surface.blit(tempsurface, (0, accumulated_height))
            elif justification == 1:
                surface.blit(tempsurface, ((rect.width - tempsurface.get_width()) / 2, accumulated_height))
            elif justification == 2:
                surface.blit(tempsurface, (rect.width - tempsurface.get_width(), accumulated_height))
            else:
                raise TextRectException, "Invalid justification argument: " + str(justification)
        accumulated_height += font.size(line)[1] + line_spacing

    return surface.subsurface(pygame.Rect((0,0,rect.width,accumulated_height - line_spacing)))

def get_transportation_data():
    
    print("Getting transportation information . . .")

    #print(lat, lon)
    hafas_link='http://demo.hafas.de/bin/pub/vbb-fahrinfo/relaunch2011/extxml.exe/'
    print(hafas_link)

    xml_string = """<?xml version="1.0" encoding="iso-8859-1"?> <ReqC lang="{0}" prod="{1}" ver="1.1" accessId="{2}">
<STBReq boardType="{3}" maxStops="{4}">
<Now />
<TableStation externalId="{5}"/>
</STBReq>
</ReqC>
""".format(lang.upper(), config.prod.lower(), config.accessId, config.boardType.upper(), config.maxStops, config.stationId);

    req = urllib2.Request(
            url=hafas_link,
            data=xml_string,
            headers={'Content-Type': 'application/xml'})

    transportation_xml = urllib2.urlopen(req)
    transportation_data = transportation_xml.read()
    transportation_xml.close()

    dom = parseString(transportation_data)

    journeys = []
    try:
        stb_journeys = dom.getElementsByTagName('STBJourney')
        for j in stb_journeys:
            main_stop = j.getElementsByTagName('MainStop')[0]
            station = main_stop.getElementsByTagName('Station')[0].attributes['name'].firstChild.nodeValue
            time = main_stop.getElementsByTagName('Time')[0].firstChild.nodeValue
            attributes = j.getElementsByTagName('JourneyAttribute')
            values = {}
            for i in attributes:
                values[str(i.firstChild.attributes['type'].firstChild.nodeValue)] = i.firstChild.firstChild.firstChild.firstChild.nodeValue
            journeys.append({"station":station, "time":time, "values":values})

    except AttributeError as error:
        print("Error getting current station information: " + str(error))

    display(journeys)


def display(journeys):
    
    print("Creating image . . .")
    
    pygame.display.init()
    pygame.font.init()
    pygame.mouse.set_visible(False)

    white = (255, 255, 255)
    black = (0, 0, 0)
    gray = (125, 125, 125)

    #display = pygame.display.set_mode((600, 800))
    display = pygame.display.set_mode((800, 600), pygame.FULLSCREEN)
    screen = pygame.Surface((600, 800))
    #screen.fill((30, 40, 125))
    screen.fill(white)

    tiny_font = pygame.font.Font("Cabin-Regular.otf", 18)
    small_font = pygame.font.Font("Fabrica.otf", 22)
    font = pygame.font.Font("Forum-Regular.otf", 36)
    #font = pygame.font.Font("The Northern Block - Lintel.otf", 32)
    symbols = pygame.font.Font("Comfortaa-Regular.otf", 12)
    comfortaa = pygame.font.Font("Comfortaa-Regular.otf", 60)
    comfortaa_small = pygame.font.Font("Comfortaa-Regular.otf", 35)

    # Dividing lines
    pygame.draw.line(screen, gray, (10, 90), (590, 90))

    station = ""
    now = ""
    y_ofs = 110

    for j in journeys:

        # Display one journey

        if (station == ""):
            station = j["station"]

        if (now == ""):
            now = j["time"].split("T")[0]

        time = small_font.render(j["time"].split("T")[1], True, black, white)
        time_rect = time.get_rect()
        time_rect.center = 60, y_ofs

        values = j["values"]
        image = pygame.transform.smoothscale(pygame.image.load("icons/" + values["INTERNALCATEGORY"].lower() + ".gif"), (25, 25))
        image.set_colorkey((255, 255, 255))
        image_rect = image.get_rect()
        image_rect.center = (130, y_ofs - 2)

        number = small_font.render(values["NUMBER"], True, black, white)
        number_rect = number.get_rect()
        number_rect.centery = y_ofs
        number_rect.left = 150

        direction = tiny_font.render(values["DIRECTION"], True, black, white)
        direction_rect = direction.get_rect()
        direction_rect.centery = y_ofs - 3
        direction_rect.left = 220

        screen.blit(time, time_rect)
        screen.blit(image, image_rect)
        screen.blit(number, number_rect)
        screen.blit(direction, direction_rect)

        y_ofs += 27

    station = font.render(station, True, black, white)
    station_rect = station.get_rect()
    station_rect.centerx = 300
    station_rect.top = 15
    screen.blit(station, station_rect)

    if (now != ""):
        now = datetime.strptime(now,"%d.%m.%y")
        if lang == "de":
            date_str = trans(now.strftime("%A")) + now.strftime(", %e. ") + months[now.month - 1] + now.strftime(" %Y")
        else:
            date_str = now.strftime("%B %e, %Y, ")
        date = tiny_font.render(date_str, True, black, white)
        date_rect = date.get_rect()
        date_rect.centerx = 300
        date_rect.top = station_rect.bottom + 3
        screen.blit(date, date_rect)


    if lang == "de":
        time_str = datetime.now().strftime("%k:%M")
    else:
        time_str = datetime.now().strftime("%l:%M%P")
    #update_time = trans("Last updated at") + " " + time_str
    update_time = time_str
    last_update = tiny_font.render(update_time, True, gray, white)
    last_update_rect = last_update.get_rect()
    last_update_rect.bottomright = (595,800)
    screen.blit(last_update, last_update_rect)

    # Rotate the display to portrait view (on the Kobo)
    graphic = pygame.transform.rotate(screen, 90)
    display.blit(graphic, (0, 0))
    pygame.display.update()

    # Show on desktop
    #graphic = pygame.transform.rotate(screen, 0)
    #display.blit(graphic, (0, 0))
    #pygame.display.update()

    while (pygame.event.wait().type != KEYDOWN): pass
    
    #call(["./full_update"])
    convert_to_raw(screen)
    call(["/mnt/onboard/.apps/koboWeather/full_update.sh"])
    
    
#try:

# read configuration
lang = config.lang.lower()
get_transportation_data()
#except IndexError as error:
    #index_error(error)
